package chrisxue815;

import java.util.Calendar;
import java.util.Set;

public interface IPerson {

	public enum Gender{Male, Female}
	public enum ShiftPattern{Day, Night}
	
	String getName();
	void setName(String name);
	Gender getGender();
	void setGender(Gender gender);
	Calendar getDateOfBirth();
	void setDateOfBirth(Calendar dateOfBirth);
	int getHeight();
	void setHeight(int height);
	int getWeight();
	void setWeight(int weight);
	boolean isEmployed();
	void setEmployed(boolean employed);
	ShiftPattern getShiftPattern();
	void setShiftPattern(ShiftPattern shiftPattern);
	boolean isSmoke();
	void setSmoke(boolean smoke);
	String getDistrict();
	void setDistrict(String district);

	int getAge();
	Set<Person> findPotentialCouples();

}
