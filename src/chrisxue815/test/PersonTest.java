package chrisxue815.test;

import java.util.GregorianCalendar;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import chrisxue815.IPerson;
import chrisxue815.IPerson.Gender;
import chrisxue815.IPerson.ShiftPattern;
import chrisxue815.Person;

public class PersonTest {

	private Person a;
	
	@Before
	public void setUp() {
		IPerson a = new Person();
		a.setName("a");
		a.setGender(Gender.Male);
		a.setDateOfBirth(new GregorianCalendar(1991, 8, 15));
		a.setHeight(181);
		a.setWeight(62);
		a.setEmployed(true);
		a.setShiftPattern(ShiftPattern.Day);
		a.setSmoke(false);
		a.setDistrict("South Circular Road");
	}
	
	@Test
	public void testFindPotentialCouples() {
		Set<Person> potentialCouples = a.findPotentialCouples();
		for (Person b : potentialCouples) {
			Assert.assertTrue(a.getGender() != b.getGender());
			
			Person male;
			Person female;
			
			if (a.getGender() == Gender.Male) {
				male = a;
				female = b;
			}
			else {
				male = b;
				female = a;
			}
			
			int mAge = male.getAge();
			int fAge = female.getAge();
			if (mAge < 26) {
				Assert.assertTrue(fAge <= mAge + 1);
			}
			
			int mHeight = male.getHeight();
			int fHeight = female.getHeight();
			Assert.assertTrue(fHeight - 25 <= mHeight);
			Assert.assertTrue(mHeight <= fHeight + 25);
			
			if (male.isEmployed() == female.isEmployed() == true) {
				Assert.assertTrue(male.getShiftPattern() == female.getShiftPattern());
			}
			
			Assert.assertTrue(male.isSmoke() == female.isSmoke());
			
			Assert.assertTrue(male.getDistrict() == female.getDistrict());
		}
	}
	
}
