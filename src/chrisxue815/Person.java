package chrisxue815;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;

public class Person implements IPerson {
	
	private String name;
	private Gender gender;
	private Calendar dateOfBirth;
	private int height;
	private int weight;
	private boolean employed;
	private ShiftPattern shiftPattern;
	private boolean smoke;
	private String district;
	
	@Override
	public String getName() {
		return name;
	}
	@Override
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public Gender getGender() {
		return gender;
	}
	@Override
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	@Override
	public Calendar getDateOfBirth() {
		return dateOfBirth;
	}
	@Override
	public void setDateOfBirth(Calendar dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	@Override
	public int getHeight() {
		return height;
	}
	@Override
	public void setHeight(int height) {
		this.height = height;
	}
	@Override
	public int getWeight() {
		return weight;
	}
	@Override
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public boolean isEmployed() {
		return employed;
	}
	@Override
	public void setEmployed(boolean employed) {
		this.employed = employed;
	}
	@Override
	public ShiftPattern getShiftPattern() {
		return shiftPattern;
	}
	@Override
	public void setShiftPattern(ShiftPattern shiftPattern) {
		this.shiftPattern = shiftPattern;
	}
	@Override
	public boolean isSmoke() {
		return smoke;
	}
	@Override
	public void setSmoke(boolean smoke) {
		this.smoke = smoke;
	}
	@Override
	public String getDistrict() {
		return district;
	}
	@Override
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public Person() {
		
	}
	
	@Override
	public int getAge() {
		return GregorianCalendar.getInstance().get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
	}
	
	@Override
	public Set<Person> findPotentialCouples() {
		return null;
	}
}
